# digitalungdom.se

Welcome to the repository for the [website](https://digitalungdom.se/) of youth organization Digital Ungdom. The website includes a forum, information about us, and is the central for user registration.

## Installation 📥

To start the development website you need to have [Node.js](https://nodejs.org/en/download/) installed.

- Download the project by using `git clone https://github.com/digitalungdom-se/digitalungdom.se.git`

- To install our packages and dependencies use `npm install`

- Finally, start the project by using `npm run start`

Boom, _easy_, **no questions asked**. We at Digital Ungdom like it that way!

## Contribute 🖋️

So you want to contribute? **Good**, the world needs more brave troopers like you.

### But how? 🤔

If you find a typo, bug or want to add a feature, create an [issue](https://github.com/digitalungdom-se/digitalungdom.se/issues) or add a [pull request](https://github.com/digitalungdom-se/digitalungdom.se/pulls) with the code you want to add! We will take a look at what you sent in and reward you with gold and glory (or maybe just some stickers) if you help our noble cause.

### Recommended IDE 💾

[VSCodium](https://vscodium.com/) is the recommended integrated development environment, VSCode will work as well. This is due to the fact that several helpful addons that are used in project’s development are only available to VSCodium and VSCode.

### License 📝

This repository is [GNU AFFERO GENERAL PUBLIC LICENSE](./LICENSE).
